import AddTodo from "../../src/components/AddTodo.vue";
import Todos from "../../src/components/Todos.vue";
import { mount, createLocalVue } from "@vue/test-utils";

import Vuex from "vuex";

describe("AddTodo", () => {
    const wrapper = mount(AddTodo);

    const localVue = createLocalVue();
    localVue.use(Vuex);

    // input control

    test("Is there a todo and description entry", () => {
        expect(wrapper.find('[data-testid="enterTodo"]'));
    });

    // adding a new todo

    test("Add new todo", async () => {
        const newTodo = jest.fn(() => Promise.resolve());
        const store = new Vuex.Store({
            actions: {
                // mock function
                addTodo: newTodo
            }
        });
        const wrappers = mount(AddTodo, { localVue, store });
        const newName = "Deneme12";
        wrappers.find('[data-testid="enterTodo"]').element.value = newName;
        wrappers.find("form").trigger("submit.prevent");
        expect(newTodo).toHaveBeenCalled();
    });
});

describe("Todos", () => {
    const localVue = createLocalVue();
    localVue.use(Vuex);

    // delete todo
    test("Delete todo", () => {
        const deletesTodos = jest.fn(() => Promise.resolve());
        const store = new Vuex.Store({
            actions: {
                // mock function
                deleteTodo: deletesTodos
            }
        });
        const wrappers = mount(Todos, { localVue, store });
        expect(wrappers.find('[data-testid="deleteBtn"]'));
    });
});
