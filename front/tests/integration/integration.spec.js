import React from "react";
import {mount} from "enzyme";
import { AddTodo, Todos } from "../components";

function setup() {
    var todoProps = {
        todos: new Array()
    }

    var todoFormProps = {
        onSubmit: jest.fn()
    }
    const todoFormWrapper = mount(<AddTodo {...todoFormProps}/>)
    const todoWrapper = Todos(<Todo {...todoProps} />)

    return {
        todoFormWrapper,
        todoWrapper
    }
}

describe("Todo integration testing", () => {
    test("Is todos an empty array?", () => {
        const {todoWrapper} = setup();

        expect(todoWrapper.props().todos).toEqual(new Array());
    });

    test("Focus on input, write Hello my todo app", async () => {
        const {todoFormWrapper} = setup();
        const input = todoFormWrapper.find('#enterTodo');
        expect(input.simulate('focus'));

        expect(input.simulate("change", { target: { value: "Hello my todo app" }}))
    });

    test("Add the todo by clicking the button or pressing enter", () => {
        const {todoFormWrapper} = setup();
        const button = todoFormWrapper.find('#addButton');
        expect(button.simulate('click'));

        expect(todoFormWrapper.find('#enterTodo').simulate('keypress', {key: 'Enter'}))

    });

    test("Empty input after adding todo", () => {
        const {todoFormWrapper} = setup();
        const input = todoFormWrapper.find('#enterTodo');

        expect(input.simulate("change", { target: { value: "" }}))
    });
});
