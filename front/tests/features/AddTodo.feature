Feature: Test to-do page

    How to add to to-do list

  Scenario: Add a to-do
    Given To-do page opens
    When I type to-do "test1234"
    Then Should be added to the to-do list


  Scenario: Viewing to-do list
    When I type to-do "test1234"
    And To-do added
    Then To-do list should be displayed



