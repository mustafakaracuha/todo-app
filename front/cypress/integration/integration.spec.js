describe("AddTodo", () => {
    before(() => {
        cy.visit("/");
    });

    // input control
    it("Is there a todo and description entry", () => {
        cy.get("[data-testid=enterTodo]").then($text => {
            const txt = $text.text();
            cy.log(txt);
        });
    });

    // adding a new todo

    it("Add new todo", () => {
        const uuid = () => Cypress._.random(0, 1e9);
        const id = uuid();
        const enterTodo = `testname${id} go it`;
        cy.get('[data-testid="enterTodo"]').type(enterTodo);
        cy.wait(1000);
        cy.get('[data-testid="addBtn"]').click();
    });

    // delete todo

    it("Delete todo", () => {
        cy.wait(1000);
        cy.get('[data-testid="deleteBtn"]').click({ multiple: true });
    });
});
