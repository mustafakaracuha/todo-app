module.exports = {
    devServer: {
        proxy: {
            '/todos': {
                target: 'http://todo-server:5000',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/todos': '/todos'
                }
            }
        }
    }
};