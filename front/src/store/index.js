import Vue from "vue";
import Vuex from "vuex";

import todoModule from "../store/modules/todo-module";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        todoModule
    }
});
