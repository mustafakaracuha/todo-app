import axios from "axios";

const url = "/todos";

const state = { 
    todo: []
};
const getters = {
    todoList: state => state.todo
};

const actions = {
    async fetchTodo({ commit }) {
        const response = await axios.get(url);
        commit("setTodo", response.data);
    },
    async addTodo({ commit }, todo) {
        const response = await axios.post(url, todo);
        commit("addNewTodo", response.data);
    },
    async deleteTodo({ commit }, todo) {
        await axios.delete(`${url}/${todo._id}`);
        commit("removeTodo", todo);
    }
};

const mutations = {
    setTodo: (state, todos) => (state.todo = todos),
    addNewTodo: (state, todo) => state.todo.unshift(todo),
    removeTodo: (state, todos) =>
        state.todo.splice(state.todo.indexOf(todos), 1)
};

export default {
    state,
    getters,
    actions,
    mutations
};
