import mongoose from "mongoose";

const todosSchema = mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
});

const Todo = mongoose.model("todo", todosSchema);

export default Todo;
