import express from "express";
import mongoose from "mongoose";
import Todo from "../db/todos.js";

const router = express.Router();

// all todo

router.get("/", async (req, res) => {
  try {
    const allTodos = await Todo.find();
    res.json(allTodos);
  } catch (error) {
    console.log(error);
  }
});

// todo add

router.post("/", async (req, res) => {
  try {
    const todo = req.body;
    const createdTodo = await Todo.create(todo);
    res.status(201).json(createdTodo);
  } catch (error) {
    console.log(error);
  }
});

// todo delete

router.delete("/:id", async (req, res) => {
  try {
    const { id } = req.params;
    await Todo.findByIdAndRemove(id);
    res.json({ message: "todo deleted" });
  } catch (error) {
    console.log(error);
  }
});

export default router;
