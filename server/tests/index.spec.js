const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = require("chai");
chai.should();
chai.use(chaiHttp);

const url = "http://localhost:5000";

const todoText = {
  text: "Hello my todo app",
};

it("todo card list must be an empty array", async () => {
  var response = await chai.request(url).get("/todos");
  console.log(response.body);
  expect(response.body.length.should.eql(0));
});

it("should create a todo card", async () => {
  var response = await chai.request(url).post("/todos").send(todoText);
  delete response.body._id;
  delete response.body.__v;
  expect(response.body.should.eql(todoText));
});

it("should fetch all the todos", async () => {
  const response = await chai.request(url).get("/todos");
  expect(response.body);
});
